import Foundation
import Leaf
import Vapor
import SwiftGD

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let rootDirectory = DirectoryConfig.detect().workDir
    let uploadDirectory = URL(fileURLWithPath: "\(rootDirectory)Public/uploads")
    let originalsDirectory = uploadDirectory.appendingPathComponent("originals")
    let thumbsDirectory = uploadDirectory.appendingPathComponent("thumbs")
    
    router.get { req -> Future<View> in
        let fm = FileManager()
        guard let files = try? fm.contentsOfDirectory(at: originalsDirectory, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles) else { throw Abort(.internalServerError) }
        
        let allFilenames = files.map { $0.lastPathComponent }
        let visisbleFilenames = allFilenames.filter { !$0.hasPrefix(".") }
        
        let context = ["files": visisbleFilenames]
        return try req.view().render("home", context)
    }
    
    
    router.post("upload") { req -> Future<Response> in
        struct UserFile: Content {
            var uploads: [File]
        }
        
        return try req.content.decode(UserFile.self).map(to: Response.self) { data in
            let acceptableTypes = [MediaType.png, MediaType.jpeg]
            
            for upload in data.uploads {
                guard let mimeType = upload.contentType else { continue }
                guard acceptableTypes.contains(mimeType) else { continue }
                
                let cleanedFilename = upload.filename.replacingOccurrences(of: " ", with: "-")
                let newURL = originalsDirectory.appendingPathComponent(cleanedFilename)
                _ = try? upload.data.write(to: newURL)
                
                let thumbURL = thumbsDirectory.appendingPathComponent(cleanedFilename)
                if let image = Image(url: newURL) {
                    if let resized = image.resizedTo(width: 300) {
                        resized.write(to: thumbURL)
                    }
                }
            }
            
            return req.redirect(to: "/")
        }
    }
}
